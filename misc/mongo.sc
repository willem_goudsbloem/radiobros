import reactivemongo.api.MongoDriver
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.bson.BSONDocument
import reactivemongo.core.nodeset.Authenticate
import scala.concurrent.ExecutionContext.Implicits.global

val driver = new MongoDriver(Akka.system)
val dbName = "radiobros"
val userName = "test"
val password = "test123"
val creds = List(Authenticate(dbName, userName, password))
val conn = driver.connection(List("ds035563.mongolab.com:35563"), authentications = creds)
val db = conn.db("radiobros")
val coll = db.collection[BSONCollection]("testcoll")
val doc = coll.find(BSONDocument("name" -> "name1"))
val cursor = doc.cursor[BSONDocument]()
val collect = cursor.collect[List](1)
collect.foreach(println)
