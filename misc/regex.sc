import java.util.Comparator

val html =
  """
    <tbody>
    <tr>
    <td class="header"><b>Radio station</b></td>
    <td class="header"><b>Location</b></td>
    <td colspan="2" class="header"><b>Listen Live</b></td>
    <td class="header"><b>Format/Comments</b></td></tr>
    
    
    <tr>
    <td><a href="http://www.tvr.by/"><b>BR1</b></a> </td>
    <td>Minsk</td>
    <td><img src="wma.gif" width="12" height="12" alt="Windows Media"></td>
    <td><a href="http://www.tvr.by/liveair/radio1.asx">128 Kbps</a></td>
    <td>News/Features</td>
    </tr>
    
    
    <tr>
    <td><a href="http://www.tvr.by/"><b>Radio Station Belarus</b></a></td>
    <td>Minsk</td>
    <td><img src="wma.gif" width="12" height="12" alt="Windows Media"></td>
    <td><a href="http://www.tvr.by/liveair/radiobel.asx">128 Kbps</a></td>
    <td>International service</td>
    </tr>
    
    <tr>
    <td><a href="http://www.radiusfm.by/"><b>Radius-FM</b></a></td>
    <td>Minsk</td>
    <td><img src="wma.gif" width="12" height="12" alt="Windows Media"></td>
    <td><a href="http://www.tvr.by/liveair/radius.asx">128 Kbps</a></td>
    <td>Pop/Rock</td>
    </tr>
    
    </tbody>
  """

val urlRegex = """(?s)<tr>(.*?)(</tr>?)""".r


val a = for (m <- urlRegex findAllMatchIn html) yield m


val entryRegex = """<tr>(?:.*?)<a href="(.*?)"><b>(.*?)</b>(?:.*?)<td>(.*?)</td>(?:.*?)alt="(.*?)"(?:.*?)<a href="(.*?)"(?:.*?)<td>(.*?)</td>""".r.unanchored
//
//
a.foreach(b => {
  b.toString().replaceAll("\\R", "") match {
    //case entryRegex(_*) => print("Entry Match")
    case entryRegex(homepageUrl, stationName, location, streamFormat, streamUrl, genre) => print(homepageUrl+ stationName+ location+ streamFormat+ streamUrl + genre + "\n")
    case _ => "none"
  }
})

class Station(var name:String, var url:String){
   override def equals(station: Any) = station match {
    case that:Station => that.url.equals(this.url)
    case _ => false
  }
  override def hashCode = url.toUpperCase.hashCode
}
val set1 = scala.collection.mutable.Set[Station]()

set1 += new Station("radio538", "a")
set1 += new Station("radio10", "a")
set1 += new Station("radio10", "b")
for(station <- set1) println(station.name)

