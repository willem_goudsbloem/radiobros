name := "radiobros"

version := "1.0"

scalaVersion := "2.11.7"

//resolvers += "Typesafe Repo" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies ++= Seq(
  "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test",
  "org.mongodb" %% "casbah" % "2.8.2"
)
