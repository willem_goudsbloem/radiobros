package com.radiobros.crawler

import scala.collection._

/**
 * Created by Willem Goudsbloem 7/31/2015.
 */
object ListenliveDotEu {
  /**
   * return the name of the site
   * @return
   */
  def getSiteName: String = "listenlive.eu"

  /**
   * return the absolute url where to base from
   * @return
   */
  def getBaseURL: String = "http://www.listenlive.eu"

  /**
   * return the pages path containing the url(s)
   * @return
   */
  def getStationPageUrl: Array[String] = {
    val countries = Array[String]("albania.html", "andorra.html","armenia.html","austria.html","azerbaijan.html","belarus.html","belgium.html","bosnia.html","bulgaria.html","croatia.html","cyprus.html","czech-republic.html","denmark.html","estonia.html","faroe.html","finland.html","france.html","georgia.html","germany.html","gibraltar.html","greece.html","hungary.html","iceland.html","ireland.html","italy.html","kosovo.html","latvia.html","liechtenstein.html","lithuania.html","luxembourg.html","macedonia.html","malta.html","moldova.html","monaco.html","montenegro.html","netherlands.html","norway.html","poland.html","portugal.html","romania.html","russia.html","san-marino.html","serbia.html","slovakia.html","slovenia.html","spain.html","sweden.html","switzerland.html","turkey.html","ukraine.html","uk.html","vatican.html")
    countries.map(country => getBaseURL+"/"+country)
  }

  /**
   * Returns all station information found in the the passed html
   * @param html
   * @return Collection(Array) with Station Objects
   */
  def getStations(html:String): immutable.Set[Station] = {
    val rowRegex = """(?s)<tr>(.*?)(</tr>?)""".r
    val rows = for (row <- rowRegex findAllMatchIn html) yield row
    val subjectRegex = """<tr>(?:.*?)<a href="(.*?)"><b>(.*?)</b>(?:.*?)<td>(.*?)</td>(?:.*?)alt="(.*?)"(?:.*?)<a href="(.*?)"(?:.*?)<td>(.*?)</td>""".r.unanchored
    val stations = mutable.Set[Station]()
    rows.foreach(b => {
      b.toString().replaceAll("\\R", "") match {
        case subjectRegex(homepageUrl, stationName, location, streamFormat, streamUrl, genre) => stations += new Station(stationName, streamUrl)
        case _ => ()
      }
    })
    //convert mutable Set to immutable
   immutable.Set[Station](stations.toSeq:_*)
  }

  /**
   * live testing
   * @deprecated as this should never be called directly from the code of course
   * @param args
   */
  def main(args: Array[String]) {
    val html = io.Source.fromURL("http://www.listenlive.eu/belarus.html")("ISO-8859-1").mkString
    println(html)
   // val stations = getStations(html)
    //stations.foreach(station => println(station.name+":"+station.streamUrl))
  }

}
