package com.radiobros.crawler

import java.net.URL
import StationPersistence._

/**
 * Created by Willem Goudsbloem on 04/09/15.
 *
 * Main class to start the process
 */
object PageCrawler {

  def main(args: Array[String]) {
    val page2crawl = ListenliveDotEu
    page2crawl.getStationPageUrl.foreach(url => {
      try {
        println(url)
        save(page2crawl.getStations(retrieve(new URL(url))))
      } catch {
        case e: Exception => e.printStackTrace()
      }
    }
    )
  }

  /**
   * retrieve the page from the passed URL in String format
   * @param url
   * @return the page
   */
  def retrieve(url: URL): String = io.Source.fromURL(url)("ISO-8859-1").mkString

}
