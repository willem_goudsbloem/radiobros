package com.radiobros.crawler

import com.mongodb.ServerAddress

import scala.util.{Failure, Success}

import com.mongodb.casbah.Imports._
import scala.collection._


/**
 * Created by Willem Goudsbloem on 18/08/15.
 */
class Station(var name: String, var streamUrl: String) {

  /**
   * Overriding the equals so that there is only one unique instance in a collection
   * based on the streamUrl
   * @param station
   * @return
   */
  override def equals(station: Any) = station match {
    case that: Station => that.streamUrl.equals(this.streamUrl)
    case _ => false
  }

  override def hashCode = streamUrl.toUpperCase.hashCode

}

object StationPersistence {


    val dbName = "radiobros"
    val userName = "test"
    val password = "test123".toCharArray
  val server = new ServerAddress("ds035563.mongolab.com", 35563)
  val creds = MongoCredential.createCredential(userName, dbName, password)
  val mongoClient = MongoClient(server, List(creds))
  val db = mongoClient("radiobros")
  val stations = db("stations")
  stations.remove(MongoDBObject.newBuilder.result)

  def save(stationSet: Set[Station]) = {
    //drop all documents first

      val builder = stations.initializeUnorderedBulkOperation
      stationSet.foreach(station => {
        builder.insert(MongoDBObject("name" -> station.name, "streamUrl" -> station.streamUrl))
        println("save $station", station.name)
      })
      builder.execute()
    }

  def main(args: Array[String]) {
    stations.remove(MongoDBObject.newBuilder.result)
    save(Set[Station](new Station("name01", "url01")))
  }

}
