package com.radiobros.crawler

import java.net.URL

import scala.util.matching.Regex

/**
 * Created by Willem Goudsbloem on 22/07/15.
 *
 * Generic Page Crawler
 * @todo not realy used, for now we use specific sites where we pre-defined the parsing regex e.g. ListenLiveDotEu.class
 */
object Crawler {

  /**
   * retrieve the page from the passed URL in String format
   * @param url
   * @return the page
   */
  def retrieve(url: URL): String = io.Source.fromURL(url)("UTF-8").mkString

  /**
   * return an collection with link urls found
   * @param html
   * @return
   */
  def searchUrl(html: String): Iterator[String] = {
    val urlRegex = """(?:"|')((http(s)?)?:?\/{0,2}[:?=A-Za-z0-9:\/\._-]*(?=\/)[:?=A-Za-z0-9:\/\._-]*)(?:"|')""".r
    //the following code removes the quotes from the result
    for (s <- urlRegex.findAllIn(html))  yield s.substring(1,s.length-1)
  }

  /**
   * returns an collection of urls that match the given String
   * @param iterator
   * @param extension
   * @return
   */
  def searchByExtension(a: Array[String], extension: String): Iterator[String]  = {
    val ext = if (extension.contains(".")) extension else "." + extension
    for (s <- a.iterator; if s.contains(ext)) yield s
  }

  def main(args: Array[String]) {
    val html = Crawler.retrieve(new URL(args(0)))
    //println(html)
    val urls:Array[String] = searchUrl(html).toArray
    searchByExtension(urls, ".m3u").foreach(println)

  }

}
