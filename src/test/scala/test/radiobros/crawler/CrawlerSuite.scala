package test.radiobros.crawler

import java.net.URL

import com.radiobros.crawler._
import org.scalatest.{FunSuite, BeforeAndAfter}

import com.sun.net.httpserver.{HttpServer}

import scala.util.matching.Regex

/**
 * Created by Willem Goudsbloem on 22/07/15.
 */
class CrawlerSuite extends FunSuite with BeforeAndAfter {

  val html =
    """
      |    <html>
      |      <head>
      |        <title>test page</title>
      |        <script src="/jsfiles/someOtherJsFile.js"></script>
      |        <script src="jsfiles/someJsFile.js"></script>
      |        <script>window.jQuery || document.write('<script src="/static/js/libs/jquery-1.8.2.min.js"><\/script>')</script>
      |        <script src="/JS/gigya.js?apiKey=3_HUfjXzNp2ktR6RyzA5r3cMXL-6_b-dbM1ut_1e_SK4n-KB3R-Zsk3_E054ISeJTy"></script>
      |       </head>
      |       <body>
      |           <a href="//test.com/help"></a>
      |           <a href="https://google.com"></a>
      |           <a href="https://google.com/goog.jpeg?arg1=hello"></a>
      |           <a href='https://google.com/goog.jpeg?arg1=hello'></a>
      |           <img src="http://image/abc.xyz.com/image.js" />
      |		        <input value="txxxt" />
      |       </body>
      |     </html>
    """.stripMargin

  val port = 8100
  val server = new SimpleHttpServer(port)

  before {

  }

  after{

  }

  test("the retrieve function should return the same html") {
    server.start(html)
    val url = new URL("http://localhost:"+port)
    val string = Crawler.retrieve(url)
    assert(string == html)
    server.stop()
  }

  test("the searchUrl function should return an array of url found in test html"){
    val arrayString:Array[String] = Crawler.searchUrl(html).toArray
    assert(arrayString(0)==="/jsfiles/someOtherJsFile.js")
    assert(arrayString(1)==="jsfiles/someJsFile.js")
    assert(arrayString(2)==="/static/js/libs/jquery-1.8.2.min.js")
    assert(arrayString(3)==="/JS/gigya.js?apiKey=3_HUfjXzNp2ktR6RyzA5r3cMXL-6_b-dbM1ut_1e_SK4n-KB3R-Zsk3_E054ISeJTy")
    assert(arrayString(4) === "//test.com/help")
    assert(arrayString(5) === "https://google.com")
    assert(arrayString(6) ===  ("https://google.com/goog.jpeg?arg1=hello"))
    assert(arrayString(7) ===  ("https://google.com/goog.jpeg?arg1=hello"))
    assert(arrayString(8) ===  ("http://image/abc.xyz.com/image.js"))
  }


  test("the searchByExtension function should return a collection of url containing the passed extension") {
    val url = Array("http://path1/file.ext3", "http://path1/noext", "//path1/hiddenextension.test?arg0=jaskldfhashdfuqw1234123")
    val arr :Array[String]= Crawler.searchByExtension(url, "ext3").toArray
    assert(arr(0).contains("http://path1/file.ext3"), "the array contains a url with the extension")
    val arr2 :Array[String]= Crawler.searchByExtension(url, ".ext3").toArray
    assert(arr2(0).contains("http://path1/file.ext3"), " the array contains a url even when dot is already added to the extension")
  }

}
