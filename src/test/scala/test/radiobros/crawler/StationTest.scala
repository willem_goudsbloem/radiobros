package test.radiobros.crawler

import com.radiobros.crawler.Station
import org.scalatest.{FunSuite}

/**
 * Created by Willem Goudsbloem on 18/08/15.
 */
class StationTest extends FunSuite {

  test("Station should override the equals method"){
    val station01 = new Station("name1", "url1")
    val station02 = new Station("name2", "url2")
    val station03 = new Station("name3", "url1")
    assert(!station01.equals(station02))
    assert(station01.equals(station03))
  }

}
