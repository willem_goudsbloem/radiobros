package test.radiobros.crawler

import java.io.{InputStream, OutputStream}
import java.net.InetSocketAddress

import com.sun.net.httpserver.{HttpExchange, HttpHandler, HttpServer}

/**
 * Created by Willem Goudsbloem on 7/23/2015.
 */
class SimpleHttpServer(port:Int) {

  val server = HttpServer.create(new InetSocketAddress(port), 0)

  def start(html:String) = {
    server.createContext("/", new RootHandler(html))
    server.setExecutor(null)
    server.start()
    println("server started at port " + port)
    server
  }

  def stop() = {
    server.stop(0)
    println("server stopped")
  }

}

class RootHandler(html:String) extends HttpHandler {

  def handle(t: HttpExchange) {
    sendResponse(t)
  }

  private def sendResponse(t: HttpExchange) {
    t.sendResponseHeaders(200, html.length())
    val os = t.getResponseBody
    os.write(html.getBytes)
    os.close()
  }

}
