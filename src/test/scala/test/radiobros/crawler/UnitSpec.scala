package test.radiobros.crawler

import org.scalatest._

/**
 * Created by willem on 22/07/15.
 */

abstract class UnitSpec extends FlatSpec with Matchers with
OptionValues with Inside with Inspectors
