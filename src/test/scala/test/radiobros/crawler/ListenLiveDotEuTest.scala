package test.radiobros.crawler


import java.net.URL

import com.radiobros.crawler.ListenliveDotEu
import org.scalatest.FunSuite

/**
 * Created by Willem Goudsbloem on 24/08/15.
 */
class ListenLiveDotEuTest extends FunSuite {

  val html =
    """
    <tr>
    <td><a href="http://www.radio350.nl/"><b>Radio 350</b></a></td>
    <td>Rijssen</td>
    <td><img src="mp3.gif" width="12" height="12" alt="MP3" /></td>
    <td><a href="http://www.radio350.nl/live/radio350.pls">128 Kbps</a></td>
    <td>Local service</td>
    </tr>
    <tr>
    <td><a href="http://www.radio538.nl/"><b>Radio 538</b></a> </td>
    <td>Hilversum</td>
    <td><img src="mp3.gif" width="12" height="12" alt="MP3" /></td>
    <td><a href="http://vip-icecast.538.lw.triple-it.nl/RADIO538_MP3.m3u">128 Kbps</a></td>
    <td>Top 40</td>
    </tr>
    """

  val lde = ListenliveDotEu

  test("getStation should return the expected stations") {

    val stations = lde.getStations(html)
    for (station <- stations) {
      if (station.name === "Radio 350") {
        assert(station.streamUrl === "http://www.radio350.nl/live/radio350.pls")
      } else {
        assert(station.streamUrl === "http://vip-icecast.538.lw.triple-it.nl/RADIO538_MP3.m3u")
      }
    }
  }

  test("getStationPageUrl should return a collection that contains all country urls") {
    lde.getStationPageUrl.foreach(println)
  }

}
